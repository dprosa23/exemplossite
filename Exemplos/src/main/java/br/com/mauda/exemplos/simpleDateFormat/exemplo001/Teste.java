package br.com.mauda.exemplos.simpleDateFormat.exemplo001;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

public class Teste { 
	
	@Test
	public void teste(){
		Date data = new Date();//Pega a data atual
		
		System.out.println("\nFormato: " + "yyyy.MM.dd G 'at' HH:mm:ss z");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
		System.out.println("Formato Aplicado: " + sdf.format(data));

		System.out.println("\nFormato: " + "EEE, MMM d, ''yy");
		sdf = new SimpleDateFormat("EEE, MMM d, ''yy");
		System.out.println("Formato Aplicado: " + sdf.format(data));

		System.out.println("\nFormato: " + "h:mm a");
		sdf = new SimpleDateFormat("h:mm a");
		System.out.println("Formato Aplicado: " + sdf.format(data));

		System.out.println("\nFormato: " + "hh 'o''clock' a, zzzz");
		sdf = new SimpleDateFormat("hh 'o''clock' a, zzzz");
		System.out.println("Formato Aplicado: " + sdf.format(data));

		System.out.println("\nFormato: " + "K:mm a, z");
		sdf = new SimpleDateFormat("K:mm a, z");
		System.out.println("Formato Aplicado: " + sdf.format(data));

		System.out.println("\nFormato: " + "yyyyy.MMMMM.dd GGG hh:mm aaa");
		sdf = new SimpleDateFormat("yyyyy.MMMMM.dd GGG hh:mm aaa");
		System.out.println("Formato Aplicado: " + sdf.format(data));

		System.out.println("\nFormato: " + "EEE, d MMM yyyy HH:mm:ss Z");
		sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
		System.out.println("Formato Aplicado: " + sdf.format(data));

		System.out.println("\nFormato: " + "yyMMddHHmmssZ");
		sdf = new SimpleDateFormat("yyMMddHHmmssZ");
		System.out.println("Formato Aplicado: " + sdf.format(data));

		System.out.println("\nFormato: " + "yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		System.out.println("Formato Aplicado: " + sdf.format(data));

		System.out.println("\nFormato: " + "yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		System.out.println("Formato Aplicado: " + sdf.format(data));

		System.out.println("\nFormato: " + "YYYY-'W'ww-u");
		sdf = new SimpleDateFormat("YYYY-'W'ww-u");
		System.out.println("Formato Aplicado: " + sdf.format(data));
	}
}