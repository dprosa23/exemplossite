package br.com.mauda.exemplos.dates.versao7.exemplo001.v2;

import java.text.ParseException;

import org.junit.Test;

public class Teste { 
	
	@Test
	public void testeOK() throws ParseException{
		ConvertDate.parseDate("08/05/2016", ConvertDate.DateFormat.BR_FORMAT);
	}
	
	@Test(expected=ParseException.class)
	public void testeDataNula() throws ParseException{
		ConvertDate.parseDate(null, ConvertDate.DateFormat.BR_FORMAT);
	}

	@Test(expected=ParseException.class)
	public void testeFormatoNulo() throws ParseException{
		ConvertDate.parseDate("08/05/2016", null); 
	}

	@Test
	public void testeDataFormatoDiferentes() throws ParseException{
		ConvertDate.parseDate("08/05/2016", ConvertDate.DateFormat.USA_FORMAT);
	}
}